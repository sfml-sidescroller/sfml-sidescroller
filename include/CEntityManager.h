#ifndef CENTITYMANAGER_H
#define CENTITYMANAGER_H

#include <CSingleton.h>

class CEntityManager : public CSingleton;
{
    public:
        CEntityManager();
        virtual ~CEntityManager();
    protected:
    private:
};

#endif // CENTITYMANAGER_H
