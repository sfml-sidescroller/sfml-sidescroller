-- window resolution
	WindowX = 800
	WindowY = 500
	fullscreen = false;
-- debug mode
	debug = {
		active = true;
		TextureManager = false;
		SoundManager = true;
		FontManager = false;
	};
-- extreme Mode (Sets the levelup-points from 200 to 20)
	extremeMode = false;
-- number of stars in the background
	backgroundStarsCount = 300
-- background style
	backgroundStarsStyle = 1
-- Delay between key presses to trigger a double click/tab (in milliseconds)
	doubleTapSpeed = 170;
-- Shoot Rate (Delay between a shot - in milliseconds)
	shootRate = 150;
-- Sound Configuration
	backgroundMusic = false;
	gameSounds = true;
-- Physic variables
	Forces = {
		normalMove = 2000;
		shiftMove = 3500;
		quickMove = 1500;
	}